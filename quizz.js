let h2 = document.createElement("h2");
h2.id = "test_status";
document.body.appendChild(h2);
let fs = document.createElement("fieldset");
fs.id = "test"
let legend = document.createElement("legend");
legend.innerText = "Leonard de Vinci";
fs.appendChild(legend);
document.body.appendChild(fs);


var pos=0,test,test_status,question,choice,choices,chA,chB,chC,correct=0;
var questions = [
  ["Question 1: What is the nationality of Leonardo da Vinci?","Italian","French","Deutch","A"],
  ["Question 2: When did Leonardo da Vinci die?","13/09/1515","15/04/1452","02/05/1519","C"],
  ["Question 3: For which french king did Leonardo da Vinci work between 1515 and 1519?","Borris the 5th","Francis 1st","Answer D","B"]
];
function recupererReponse(element){
  return document.getElementById(element);
}

function renderQuestion(){
  test=recupererReponse("test");
  if(correct == 3)
  {
    toastr.success("Congratulations! You just won the Knowledge object.");
    document.cookie = "Knowledge=true";
  }
  if(pos >=questions.length){
    test.innerHTML = "<h2 id='SpaceFooterQuizz'>You have "+correct+" good answer(s) out of "+questions.length+" questions </h2>";
    recupererReponse("test_status").innerHTML = "End of the quizz.";
    pos=0;
    correct=0;
    return false;
  }
  recupererReponse("test_status").innerHTML = "Question "+(pos+1)+" out of "+questions.length;
  question = questions[pos][0];
  chA = questions[pos][1];
  chB = questions[pos][2];
  chC = questions[pos][3];
  test.innerHTML="<h3>"+question+"</h3>";
  test.innerHTML+="<input type='radio' name='choices' value='A' style='margin-right:1em; margin-left:2em;'>"+chA+"<br>";
  test.innerHTML+="<input type='radio' name='choices' value='B' style='margin-right:1em; margin-left:2em;'>"+chB+"<br>";
  test.innerHTML+="<input type='radio' name='choices' value='C' style='margin-right:1em; margin-left:2em;'>"+chC+"<br><br>";
  test.innerHTML+="<button id='SpaceFooterQuizz' type='button' style='margin-left:2em;' onclick='checkAnswer()'> Next </button>";
  }

function checkAnswer(){
  console.log("test");
  choices= document.getElementsByName("choices");
  for(var i=0;i<choices.length; i++){
    if(choices[i].checked){
      choice=choices[i].value;
    }
  }
  if(choice == questions[pos][4]){
    correct++;
  }
  pos++;
  renderQuestion();
}
window.addEventListener("load",renderQuestion,false);
document.body.appendChild(document.getElementsByClassName("footer")[0]);
