
$(document).ready (function() {

    class Quizz{
        constructor(nom, description, questions){
            //this.id = id;
            this.nom = nom;
            this.description = description;
            this.questions = questions;
        }
    }

    class Question{
        constructor(num,intitule, reponses, correctes){
            this.num = num;
            this.intitule = intitule;
            this.reponses = reponses;
            this.correctes = correctes;
        }
    }

    class Reponse{
        constructor(lettre, reponse){
            this.lettre = lettre;
            this.reponse = reponse;
        }
    }

    $("#buttonCréer").click(formQuizz);
    $("#buttonAff").click(refreshQuizz);

    function refreshQuizz() {
        $("#currentquizz").empty();
        $.ajax({
            url: "http://localhost:3000/quizz",
            type: "GET",
            dataType : "json",
            success: function(quizzs) {
                console.log(JSON.stringify(quizzs));
                $('#quizzs').empty();
                $('#quizzs').append($('<ul>'));
                for(let i=0;i<quizzs.length;i++){
                    console.log(quizzs[i]);
                    $('#quizzs ul')
                    .append($('<li class="mb-2">')
                    .append($('<a>').text(quizzs[i].nom).on("click", quizzs[i], afficheQuizz))
                    .append($('<button class = "btn btn-secondary ml-2">').text("Modifier").on("click", quizzs[i], formModifQuizz))
                    .append($('<button class = "btn btn-danger ml-2">').text("Supprimer").on("click", quizzs[i], supprimerQuizz))
                    );
                        }
                    },
            error: function(req, status, err) {
                        $("#quizzs").html("<b>Impossible de récupérer les quizzs !</b>");
                        }
                })
    }


    function supprimerQuizz(q) {
        $.ajax({
            url: "http://localhost:3000/quizz/" + q.data.id,
            type:'DELETE',
            contentType:'application/json',
            dataType:'json',
            success:function(msg) {
                alert('Delete Success');
            },
            error:function(err){
                alert('Delete Error');
            }
        });
        refreshQuizz()
    }


    function afficheQuizz(event){
        $("#currentquizz").empty();
        quizz = event.data;
        console.log(quizz.questions);
        $("#currentquizz")
            .append($('<span class="col-12 text-center h2"><p class=name>'+ quizz.nom +  '</p><br></span>'))
            .append($('<span class="col-12 text-center h4"><p class=name>'+ quizz.description +  '</p><br></span>'))

        for (const q of quizz.questions) {
            $("#currentquizz")
                .append($("<div id="+ q.num +" class='col-6'>"))
                .append($("</div>"));
            $("#currentquizz #"+q.num)
                .append($(" <p>Question " + q.num + ":</p>"))
                .append($(" <p>"+ q.intitule +"</p>"));
            for (const r of q.reponses) {
                $("#currentquizz #"+q.num)
                    .append($("<span>"+ r.reponse +"<input class = 'ml-2' name='choices' type='checkbox' value="+ r.lettre +"><br></span>"));
            }
            console.log(q.num);

            $("#currentquizz #"+q.num)
                .append($('<input type="hidden" class="correcte" value='+ q.correctes +'>'))
                .append($("<button class ='btn btn-info'> Vérifier </button>").on("click", q.num, verifReponse)
                );
        }
    }

    function verifReponse(num){
        console.log(num.data);
        console.log(1322);
        $("#currentquizz #"+ num.data + " .rep").remove();
        reponses = $("#currentquizz #"+ num.data +" .correcte").val();
        choices = [];
        $("#currentquizz #"+ num.data + " input[name='choices']:checked").each(function(){
            choices.push($(this));
        });
        for (const rep of choices ) {
            console.log(reponses);
            if (reponses.includes(rep.val())){
                rep.after("<p class='text-success rep'> Correct </p>");
            }
            else{
                rep.after("<p class='text-danger rep'> Faux </p>");
            }
        }
    }

    function voirQuizz(event){
        $("#currentquizz").empty();
        formQuizz();
        }


    function formQuizz(isnew){
        $("#currentquizz").empty();
        $("#currentquizz")
            .append($('<span class="col-12">Nom<input class="form-control" type="text" id="nom"><br></span>'))
            .append($('<span class="col-12">Description<textarea class="form-control" id="descr"></textarea><br></span>'))
            //.append($('<span><input type="checkbox" id="reponses.size()"><br></span>'))
            //.append($('<span><input type="checkbox" id="reponses.size()+1"><br></span>'))
            //.append($('<span><input type="checkbox" id="reponses.size()+2"><br></span>'))
            .append($('<span><input type="hidden" id="turi"><br></span>'))
            .append($('<span ><input class="form-control" type="button" value="Save Quizz"><br></span>').on("click", saveNewQuizz)
                );
        }


    function saveNewQuizz(){
        var quizz = new Quizz(
            $("#currentquizz #nom").val(),
            $("#currentquizz #descr").val()
            );
        console.log(JSON.stringify(quizz));
        var idQuizz;
        var nameQuizz;
        $.ajax({
            url: "http://localhost:3000/quizz",
            type:'POST',
            contentType:'application/json',
            async: false,
            data: JSON.stringify(quizz),
            dataType:'json',
            success:function(msg) {
                alert('Save Success');
                idQuizz = msg.id;
                nameQuizz = msg.nom;
                descrQuizz = msg.description;
            },
            error:function(err){
                alert('Save Error');
            }
        });
        refreshQuizz();
        formQuestion(idQuizz, nameQuizz, descrQuizz, 1);
 
        //refreshTaskList();
    }

    var i;

    function formQuestion(idQuizz, nameQuizz, descrQuizz, idQuestion){
        i = 'A';
        $("#currentquizz").empty();
        $("#currentquizz")
            .append($('<span><h2>Nouvelle question</h2><br></span>'))
            .append($('<span class = "col-12">Intitulé<input class= "form-control" type="text" id="inti"><br></span>'))
            .append($('<span id=rep'+ i +' class = "row col-12"><p class="col-2">Reponse '+ i +' </p>  <input class= "rep form-control col-6" type="text" id='+ i +' class="rep"> <input class="ml-2" type="checkbox" value='+ i +'> <br></span>'))
            .append($('<span class = "col-12 mb-2"><input type="button" value="+"><br></span>').on("click",addReponse))
            .append($('<span><input type="hidden" id="idQuestion" value='+ idQuestion +'><br></span>'))
            .append($('<span><input type="hidden" id="idQuizz" value='+ idQuizz +'><br></span>'))
            .append($('<span><input type="hidden" id="nameQuizz" value='+ nameQuizz +'><br></span>'))
            .append($('<span><input type="hidden" id="descrQuizz" value='+ descrQuizz +'><br></span>'))
            .append($('<span class ="mr-2"><input type="button" value="Save question"><br></span>').on("click", addQuestion))
            .append($('<span><input type="button" value="Terminer Quizz"><br></span>').on("click", termineQuizz)

                );
        }

    function addReponse(){
        idRepPrec = i;
        i = nextChar(i);
        ($('<span id=rep'+ i +' class = "row col-12"><p class="col-2">Reponse '+ i +' </p>  <input class= "rep form-control col-6" type="text" id='+ i +'> <input class="ml-2" type="checkbox" value='+ i +'> <br></span>').insertAfter($("#currentquizz #rep"+ idRepPrec)));



    }

    function nextChar(c) {
        return String.fromCharCode(c.charCodeAt(0) + 1);
    }
    var questions = [];


    function addQuestion(idQuizz){
        console.log($("#currentquizz span .rep"))
        var reponses = [];
        var correctes = [];
        for (const q of $("#currentquizz span .rep")) {
            reponses.push(new Reponse(q.id, q.value));
        }

        for (const l of $("#currentquizz span input:checked")) {
            correctes.push(l.value);
        }
        console.log(correctes);
        console.log($("#currentquizz span #inti").val());
        var question = new Question(
            parseInt($("#currentquizz span #idQuestion").val()),
            $("#currentquizz span #inti").val(),
            reponses,
            correctes
        );
        questions.push(question);
        console.log($("#currentquizz span #nameQuizz").val());
        var quizz = new Quizz(
            $("#currentquizz span #nameQuizz").val(), 
            $("#currentquizz span #descrQuizz").val(),
            questions
        )
        console.log(JSON.stringify(quizz));
        console.log($("#currentquizz span #idQuizz").val());
        $.ajax({
            url: "http://localhost:3000/quizz/" + $("#currentquizz span #idQuizz").val(),
            type:'PUT',
            contentType:'application/json',
            data: JSON.stringify(quizz),
            dataType:'json',
            success:function(msg) {
                alert('Save Success');
            },
            error:function(err){
                alert('Save Error');
            }
        });
        console.log(parseInt($("#currentquizz span #idQuestion").val())+1);
        formQuestion($("#currentquizz span #idQuizz").val(), $("#currentquizz span #nameQuizz").val(), $("#currentquizz span #descrQuizz").val(), parseInt($("#currentquizz span #idQuestion").val())+1)



    }

    function termineQuizz() {
        questions = [];
        refreshQuizz();
        $("#currentquizz").empty();
        $("#currentquizz")
            .append($('<span><p>Quizz enregistré</p></span>'))
    }


    function saveModifQuizz(){
        var questions = [];
        for (const q of $("#currentquizz .question")) {
            var reponses = [];
            var correctes = [];
            for (const rep of $("#currentquizz .divQ #" + q.id+ " span  ."+ q.id)) {
                console.log(rep.value);
                reponses.push(new Reponse(rep.id, rep.value));
            }
            for (const l of $("#currentquizz .divQ #" + q.id+ " span input:checked")) {
                     correctes.push(l.value);
            }
            console.log($("#currentquizz .divQ #" + q.id+ " span .inti"));
            var question = new Question(
                     parseInt(q.id),
                     $("#currentquizz .divQ #" + q.id+ " span .inti").val(),
                     reponses,
                     correctes
            )
            questions.push(question);
        }

        var quizz = new Quizz(
                 $("#currentquizz span .name").text(), 
                 $("#currentquizz span .descr").text(),
                 questions
             )
        console.log(JSON.stringify(quizz));
        $.ajax({
            url: "http://localhost:3000/quizz/" + $("#currentquizz span #idQuizz").val(),
            type:'PUT',
            contentType:'application/json',
            data: JSON.stringify(quizz),
            dataType:'json',
            success:function(msg) {
                alert('Save Success');
            },
            error:function(err){
                alert('Save Error');
            }
        });
        refreshQuizz();



    }



    function formModifQuizz(quizz){
        console.log(quizz.data)
        var quizz = quizz.data;
        $("#currentquizz").empty();
        $("#currentquizz")
            .append($('<span class="col-12 text-center h2"><p class=name>'+ quizz.nom +  '</p><br></span>'))
            .append($('<span class="col-12 text-center h4"><p class=descr>'+ quizz.description +  '</p><br></span>'))
            .append($('<span><input type="hidden" id="idQuizz" value='+ quizz.id +'><br></span>'))
            .append($('<div class="row divQ col-12">'))
        for (const q of quizz.questions) {
            $(".divQ")
                .append($('<div class="col-6 question" id = '+q.num+'>'))
            $("#currentquizz .divQ #"+ q.num)
                .append($('<span><p>Question : ' + q.num + '</p><br></span>'))
                .append($('<span><p class=inti>Intitulé : ' + q.intitule + '</p><br></span>'))
            $('#currentquizz .divQ #'+q.num + " span .inti").val(q.intitule);
            for (const rep of q.reponses) {
                console.log(rep.reponse)
                $("#currentquizz .divQ #"+q.num)
                    .append($("<span class=row> <p class=col-3> Reponse " + rep.lettre + "</p> <input class='form-control col-5 "+ q.num+" ' type=text id ="+rep.lettre+" >  <input class=ml-2 type=checkbox value="+ rep.lettre +"  ></span><br>"))
                    .append($('</div>'))
                $("#currentquizz .divQ #" + q.num + " span #"+rep.lettre).val(rep.reponse);
                (q.correctes.includes(rep.lettre) ? $("#currentquizz .divQ #" + q.num + " span :checkbox[value="+ rep.lettre +"]").prop('checked', true) : $("#currentquizz .divQ #" + q.num + " span :checkbox[value="+ rep.lettre +"]").prop('checked', false));
            }

        }
        $("#currentquizz")
        .append($('</div>'))
        .append($('<span> <button class="btn btn-primary mr-2"> Ajouter une question </button> </span>').on("click", formAddQuestion))
        .append($('<span> <button class="btn btn-success"> Save </button> </span>').on("click", saveModifQuizz))

    } 

    function formAddQuestion() {
        i = 'A';

        var num = parseInt($('.divQ div').last().attr('id'))+1;
        $(".divQ")
            .append($('<div class="col-6 question" id = '+ num +'>'))
        $("#currentquizz .divQ #"+ num)
            .append($('<span><p>Question : ' + num + '</p><br></span>'))
            .append($('<span class="row mb-3"> <p class=" col-3">Intitulé : </p> <input class="form-control col-5 inti" type="text" > <br></span>'))
            .append($("<span id=rep"+ i +" class=row> <p class=col-3> Reponse "+ i +"</p> <input class='form-control col-5 "+num+"' type=text id ="+ i +" >  <input class=ml-2 type=checkbox value="+ i +"  ></span><br>"))
            .append($('<button class="btn btn-secondary mb-2"> Ajouter une réponse </button>').on("click", num, addQuestionModif))
        


    }

    function addQuestionModif(num) {
        idRepPrec = i;
        i = nextChar(i);
        console.log();
        ($('<span id=rep'+ i +' class = "row"><p class="col-3">Reponse '+ i +' </p>  <input class= "rep form-control col-5 '+num.data+' type="text" id='+ i +'> <input class="ml-2" type="checkbox" value='+ i +'> <br></span>').insertAfter($("#currentquizz .divQ #"+ num.data + " #rep" + idRepPrec)));
    }




        

});
